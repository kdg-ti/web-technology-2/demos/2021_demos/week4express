const express = require("express");
const personsData = require("../data/persons")

const router = express.Router();

router.get("/", (req, res)=>{
    if (req.query.gender) {
        if (req.query.gender!='M'&&req.query.gender!='F'&&req.query.gender!='X') {
            res.status(400)
            res.json({error:"shoudl eb M,F or X"});
        } else {
            res.json(personsData.persons.filter(p=>p.gender===req.query.gender));
        }
    } else {
        res.json(personsData.persons);
    }
})

router.get("/:personid", (req, res)=> {
    let personid = parseInt(req.params.personid)
    if (!personid) {
        res.status(400)
        res.json({error: "bad personid:" + req.params.personid});
    } else {
        let person = personsData.persons.find(p => p.id === personid);
        if (!person) {
            res.sendStatus(404)
        } else {
            res.json(personsData.persons.find(p => p.id === personid));
        }
    }
})

router.use(express.json())

router.put("/", (req, res)=>{
    console.log(req.body);
    //verwerken...
})

module.exports = router;