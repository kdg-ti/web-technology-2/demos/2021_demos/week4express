const express = require("express");
const morgan = require("morgan");
const personsRouter = require("./routes/personRouter")

const app = express();
const port = 3000;

app.listen(port, () => {
    console.log("Server listening on port " + port)
})

// app.use((req, res, next)=> {
//     console.log("middelware running!");
//     next();
// })
app.use(morgan('dev'));
app.use(express.static("public"));
app.use('/persons', personsRouter)



app.get("/", (req, res) => {
    res.send("Hello world!");
})